-- This seems to still work without this line, the needed functions are probably already in the global space.
local assets =
{
	Asset("ANIM", "anim/x_marks_spot.zip"),
}

local prefabs =
{
	"messagebottle",
	"collapse_small",
}

AllBuriedtreasure = {} -- save Buriedtreasure

local function onfinishcallback(inst, worker)
    inst.MiniMapEntity:SetEnabled(false)
    inst:RemoveComponent("workable")
    --inst.components.hole.canbury = true

	if worker then		
		-- figure out which side to drop the loot
		local pt = Vector3(inst.Transform:GetWorldPosition())
		local hispos = Vector3(worker.Transform:GetWorldPosition())

		local he_right = ((hispos - pt):Dot(TheCamera:GetRightVec()) > 0)
		
		if he_right then
			inst.components.lootdropper:DropLoot(pt - (TheCamera:GetRightVec()*(math.random()+1)))
			inst.components.lootdropper:DropLoot(pt - (TheCamera:GetRightVec()*(math.random()+1)))
		else
			inst.components.lootdropper:DropLoot(pt + (TheCamera:GetRightVec()*(math.random()+1)))
			inst.components.lootdropper:DropLoot(pt + (TheCamera:GetRightVec()*(math.random()+1)))
		end

		inst.SoundEmitter:PlaySound("ia/common/loot_reveal")

		if not c_findnext("moonrockseed") and math.random() < TheWorld.state.cycles/100 then
			inst.loot = "moonrockseed"
		end
		if not c_findnext("terrarium") and math.random() < TheWorld.state.cycles/100 then
			inst.loot = "terrarium"
		end

		SpawnTreasureChest(inst.loot, inst.components.lootdropper, inst:GetPosition(), inst.treasurenext)
		inst:Remove()
	end
end

local function OnSave(inst, data)
    data.loot = inst.loot
	data.revealed = inst.revealed
end

local function OnLoad(inst, data)
    if data and data.loot then
        inst.loot = data.loot
		inst.revealed = data.revealed
    end

    if inst.revealed then
		inst:Reveal()
	end
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	local minimap = inst.entity:AddMiniMapEntity()
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()

	inst:AddTag("buriedtreasure")
	inst:AddTag("NOCLICK")

	minimap:SetIcon( "xspot.tex" )
	minimap:SetEnabled(false)

    anim:SetBank("x_marks_spot")
    anim:SetBuild("x_marks_spot")
    anim:PlayAnimation("anim")

	inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

	inst.entity:Hide()
    inst:AddComponent("inspectable")

    inst.components.inspectable.getstatus = function(inst)
        if not inst.components.workable then
            return "DUG"
        end
    end

	inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnFinishCallback(onfinishcallback)

	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot({"boneshard"})


    inst.loot = ""
    inst.revealed = false

    inst.Reveal = function(inst)
    	print("Treasure revealed")
    	inst.revealed = true
    	inst.entity:Show()
    	inst.MiniMapEntity:SetEnabled(true)
    	inst:RemoveTag("NOCLICK")
        table.removearrayvalue(AllBuriedtreasure, inst)
	end

	inst.IsRevealed = function(inst)
		return inst.revealed
	end

	inst.SetRandomTreasure = function(inst)  --for luck hat
		inst:Reveal()
		local treasures = GetTreasureLootDefinitionTable()
		local treasure = GetRandomKey(treasures)
		inst.loot = treasure
	end

	inst.SetRandomNewTreasure = function(inst)
		inst:Reveal()
		local treasures = GetNewTreasures()
		local treasure = GetRandomKey(treasures)
		inst.loot = treasure
		if inst.loot == "moonrockseed" then
			inst.loot = "moonworker"
		end
		if inst.loot == "terrarium" then
			inst.loot = "moongardener"
		end
	end

	table.insert(AllBuriedtreasure, inst)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    return inst
end

return Prefab( "buriedtreasure", fn, assets, prefabs )
