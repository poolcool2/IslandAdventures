local assets = {
    Asset("ANIM", "anim/merm_sw_house.zip"),
    Asset("MINIMAP_IMAGE", "mermhouse_tropical"),
}

local sw_loot = 
{
    "boards",
    "rocks",
    "pondfish_tropical",
}

local sw_loot_merm = 
{
    "fish_tropical",
}

local prefabs = {
    "mermhouse"
}
local original_function_spawn
local function new_function_spawn(inst, child)
    if not inst:HasTag("burnt") then
        child.components.lootdropper:SetLoot(sw_loot_merm)
        child:AddTag("save_loot")
    end
    original_function_spawn(inst, child)
end

local function fn()
    local inst = Prefabs["mermhouse"].fn()

    inst.AnimState:SetBank("merm_sw_house")
    inst.AnimState:SetBuild("merm_sw_house")
    inst.MiniMapEntity:SetIcon("mermhouse_tropical.tex")
    
    inst.realprefab = "mermhouse_tropical"

    inst:SetPrefabName("mermhouse")

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("named")
    inst.components.named:SetName(STRINGS.NAMES.MERMHOUSE_TROPICAL)

    inst.components.lootdropper:SetLoot(sw_loot)
    if not original_function_spawn then
        original_function_spawn = inst.components.childspawner.onspawned
    end
    inst.components.childspawner.onspawned = new_function_spawn
    return inst
end

return Prefab("mermhouse_tropical", fn, assets, prefabs)
