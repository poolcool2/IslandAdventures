FrontEndAssets = {
    Asset("IMAGE", "images/customization_shipwrecked.tex"),
    Asset("ATLAS", "images/customization_shipwrecked.xml"),
}

ReloadFrontEndAssets()

local _G = GLOBAL
local STRINGS = _G.STRINGS
local TheFrontEnd = _G.TheFrontEnd
local unpack = _G.unpack

modimport("main/strings")

local size_descriptions = GetCustomizeDescription("size_descriptions")
local yesno_descriptions = GetCustomizeDescription("yesno_descriptions")
local frequency_descriptions = GetCustomizeDescription("frequency_descriptions")

local rate_descriptions = {
    { text = STRINGS.UI.SANDBOXMENU.SLIDENEVER,    data = "never" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEVERYRARE, data = "veryrare" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDERARE,     data = "rare" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEDEFAULT,  data = "default" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEOFTEN,    data = "often" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEALWAYS,   data = "always" },
}

local islandquantity_descriptions = {
    { text = STRINGS.UI.SANDBOXMENU.BRANCHINGLEAST, data = "never" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDERARE,      data = "rare" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEDEFAULT,   data = "default" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEOFTEN,     data = "often" },
    { text = STRINGS.UI.SANDBOXMENU.BRANCHINGMOST,  data = "always" },
}

local worldtypes = {
    { text = STRINGS.UI.SANDBOXMENU.WORLDTYPE_DEFAULT,     data = "default" },
    { text = STRINGS.UI.SANDBOXMENU.WORLDTYPE_MERGED,      data = "merged" },
    -- { text = STRINGS.UI.SANDBOXMENU.WORLDTYPE_ISLANDS,     data = "islands" },
    { text = STRINGS.UI.SANDBOXMENU.WORLDTYPE_ISLANDSONLY, data = "islandsonly" },
}

local LEVELCATEGORY = GLOBAL.LEVELCATEGORY
local worldgen_atlas = "images/worldgen_customization.xml"
local ia_atlas = "images/customization_shipwrecked.xml"

local function add_group_and_item(category, name, text, desc, atlas, order, items)
    AddCustomizeGroup(category, name, text, desc, atlas or ia_atlas, order)
    if items then
        for k, v in pairs(items) do
            AddCustomizeItem(category, name, k, v)
        end
    end
end

local ia_customize_table = {

    islandmisc = {
        category = LEVELCATEGORY.WORLDGEN,
        text = STRINGS.UI.SANDBOXMENU.CUSTOMIZATIONPREFIX_IA..STRINGS.UI.SANDBOXMENU.CHOICEMISC,
        items = {
            primaryworldtype = { value = "default", enable = false, image = "world_map.tex", atlas = worldgen_atlas, desc = worldtypes, order = 1, world = { "forest" } },
            islandquantity   = { value = "default", enable = false, image = "world_size.tex", atlas = worldgen_atlas, desc = size_descriptions, order = 3, world = {"forest"} },
            -- islandsize      = { value = "merged", enable = false, image = "world_map.tex", desc = worldtypes, order = 1, world = {"forest"} },
            volcano          = { value = "default", enable = false, image = "volcano.tex", desc = yesno_descriptions, order = 4, world = {"forest"} },
            dragoonegg       = { value = "default", enable = false, image = "dragooneggs.tex", desc = frequency_descriptions, order = 5, world = {"forest"} },
            tides            = { value = "default", enable = false, image = "tides.tex", desc = yesno_descriptions, order = 6, world = {"forest"} },
            floods           = { value = "default", enable = false, image = "floods.tex", desc = frequency_descriptions, order = 7, world = {"forest"} },
            oceanwaves       = { value = "default", enable = false, image = "waves.tex", desc = rate_descriptions, order = 8, world = {"forest"} },
            poison           = { value = "default", enable = false, image = "poison.tex", desc = yesno_descriptions, order = 9, world = {"forest"} },
            bermudatriangle  = { value = "default", enable = false, image = "bermudatriangle.tex", desc = frequency_descriptions, order = 10, world = {"forest"} },
        }
    },

	islandunprepared = {
        category = LEVELCATEGORY.WORLDGEN,
        text = STRINGS.UI.SANDBOXMENU.CUSTOMIZATIONPREFIX_IA..STRINGS.UI.SANDBOXMENU.CHOICEFOOD,
		desc = frequency_descriptions,
        items = {
			sweet_potato = { value = "default", enable = true, image = "sweetpotatos.tex", order = 2, world = {"forest"} },
			limpets 	 = { value = "default", enable = false, image = "limpets.tex", order = 4, world = {"forest"} },
			mussel_farm  = { value = "default", enable = false, image = "mussels.tex", order = 5, world = {"forest"} },
        }
    },

	islandanimals = {
		category = LEVELCATEGORY.SETTINGS,
        text = STRINGS.UI.SANDBOXMENU.CUSTOMIZATIONPREFIX_IA..STRINGS.UI.SANDBOXMENU.CHOICEANIMALS,
		desc = frequency_descriptions,
        items = {
			wildbores = { value = "default", enable = false, image = "wildbores.tex", order = 3, world = {"forest"} },
			whalehunt = { value = "default", enable = false, image = "whales.tex", order = 7, world = {"forest"} },
			crabhole  = { value = "default", enable = false, image = "crabbits.tex", order = 8, world = {"forest"} },
			ox        = { value = "default", enable = false, image = "ox.tex", order = 9, world = {"forest"} },
			solofish  = { value = "default", enable = false, image = "dogfish.tex", order = 10, world = {"forest"} },
			doydoy    = { value = "default", enable = false, image = "doydoy.tex", desc = yesno_descriptions, order = 11, world = {"forest"} },
			jellyfish = { value = "default", enable = false, image = "jellyfish.tex", order = 12, world = {"forest"} },
			lobster   = { value = "default", enable = false, image = "lobsters.tex", order = 13, world = {"forest"} },
			--Note: This one could be linked to Birds
			seagull   = { value = "default", enable = false, image = "seagulls.tex", order = 14, world = {"forest"} },
			ballphin  = { value = "default", enable = false, image = "ballphins.tex", order = 15, world = {"forest"} },
			primeape  = { value = "default", enable = false, image = "monkeys.tex", order = 16, world = {"forest"} },
		}
	},

	islandmonsters = {
		category = LEVELCATEGORY.SETTINGS,
        text = STRINGS.UI.SANDBOXMENU.CUSTOMIZATIONPREFIX_IA..STRINGS.UI.SANDBOXMENU.CHOICEMONSTERS,
		desc = frequency_descriptions,
        items = {
			--TODO implement "Sharx" as "likelihood of sharx/crocodogs when meat lands on water" ?
			-- sharx = { value = "default", enable = false, image = "crocodog.tex", order = 1, world = {"forest"} },
			--Note: This one is houndwaves, which technically speaking already exists.
			-- crocodog = { value = "default", enable = false, image = "crocodog.tex", order = 2, world = {"forest"} },
			twister    = { value = "default", enable = false, image = "twister.tex", order = 7, world = {"forest"} },
			tigershark = { value = "default", enable = false, image = "tigershark.tex", order = 8, world = {"forest"} },
			kraken 	   = { value = "default", enable = false, image = "kraken.tex", order = 9, world = {"forest"} },
			flup 	   = { value = "default", enable = false, image = "flups.tex", order = 10, world = {"forest"} },
			mosquito   = { value = "default", enable = false, image = "mosquitos.tex", order = 11, world = {"forest"} },
			swordfish  = { value = "default", enable = false, image = "swordfish.tex", order = 12, world = {"forest"} },
			stungray   = { value = "default", enable = false, image = "stinkrays.tex", order = 13, world = {"forest"} },
		}
	},

}

for name, data in pairs(ia_customize_table) do
    add_group_and_item(data.category, name, data.text, data.desc, data.atlas, data.order, data.items)
end
_G.IACustomizeTable = ia_customize_table

_G.scheduler:ExecuteInTime(0, function() -- Delay a frame so we can get ServerCreationScreen when entering a existing world
    local servercreationscreen --= GLOBAL.TheFrontEnd:GetActiveScreen()
    for _, screen in pairs(TheFrontEnd.screenstack) do
        if screen.name == "ServerCreationScreen" then
            servercreationscreen = screen
            break
        end
    end
    if not (servercreationscreen and servercreationscreen.world_tabs) then return end

    local forest_tab = servercreationscreen.world_tabs[1]
    local cave_tab = servercreationscreen.world_tabs[2]

    if forest_tab then
        if not servercreationscreen:CanResume() then -- Only when first time creating the world
            for _, v in ipairs(forest_tab.worldsettings_widgets) do
                v:OnPresetButton("SURVIVAL_SHIPWRECKED_CLASSIC") -- Automatically try switching to the Shipwrecked Preset
            end
        end

        if not servercreationscreen:CanResume() and cave_tab:IsLevelEnabled() and GetModConfigData("openvolcano") then -- Only when first time creating the world
            servercreationscreen.world_tabs[2] = _G.deepcopy(servercreationscreen.world_tabs[1])
            cave_tab = servercreationscreen.world_tabs[2]
            if not servercreationscreen:CanResume() then
                for _, v in ipairs(cave_tab.worldsettings_widgets) do
                    v:OnPresetButton("VOLCANO_LEVEL")
                end
            end
        end

        if cave_tab and GetModConfigData("openvolcano") then
            local sublevel_adder_overlay = cave_tab.sublevel_adder_overlay
            if sublevel_adder_overlay then
                local add_cave_btn = sublevel_adder_overlay.actions.items[1]
                local cb = add_cave_btn.onclick or function() end
                add_cave_btn:SetOnClick(function(self, ...)
                    cb(unpack({self, ...}))
                    servercreationscreen.world_tabs[2] = _G.deepcopy(servercreationscreen.world_tabs[1])
                    cave_tab = servercreationscreen.world_tabs[2]
                    if not servercreationscreen:CanResume() then
                        for _, v in ipairs(cave_tab.worldsettings_widgets) do
                            v:OnPresetButton("VOLCANO_LEVEL")
                        end
                    end
                end)
            end
        end
    end
end)
