local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

---------------------Transmitting the world by volcano------------------------
LastMigrator = nil
IAENV.AddComponentPostInit("playerspawner", function(self)
	local old_SpawnAtLocation = self.SpawnAtLocation
	self.SpawnAtLocation = function(self, inst, player, x, y, z, isloading, ...)
		if player.migration ~= nil then
			LastMigrator = player
		end

		old_SpawnAtLocation(self, inst, player, x, y, z, isloading, ...)
	end
end)