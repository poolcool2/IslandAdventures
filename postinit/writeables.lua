local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local writeables = require("writeables")
local kinds = UpvalueHacker.GetUpvalue(writeables.makescreen, "kinds")

local fn = loadfile("prefabs/boats")
local boats = {fn()}
for k, prefab in ipairs(boats) do
    if not string.find(prefab.name, "placer") then
        kinds[prefab.name] = {
            prompt = "", -- Unused
            animbank = "ui_board_5x3",
            animbuild = "ui_board_5x3",
            menuoffset = Vector3(6, -70, 0),

            cancelbtn = {
                text = STRINGS.BEEFALONAMING.MENU.CANCEL,
                cb = nil,
                control = CONTROL_CANCEL
            },

            acceptbtn = {
                text = STRINGS.BEEFALONAMING.MENU.ACCEPT,
                cb = nil,
                control = CONTROL_ACCEPT
            },
        }

        kinds["player_" .. prefab.name] = kinds[prefab.name]
    end
end