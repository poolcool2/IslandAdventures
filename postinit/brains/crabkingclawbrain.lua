local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local BOAT_TAGS = {"boat"}

local function New_ShouldClamp(inst)
    if inst:IsValid() and not inst.sg:HasStateTag("busy") then
        local x,y,z = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x,y,z, 4.5, BOAT_TAGS)
        if #ents > 0 then
            for i=#ents, 1, -1 do
                if not ents[i]:IsValid() or ents[i].components.boathealth or ents[i].components.health:IsDead() then
                    table.remove(ents,i)
                end
            end
        end
        if #ents > 0 then
            inst:PushEvent("clamp",{target = ents[1]})
        end
    end
    return nil
end

CrabkingClawBrain = require("brains/crabkingclawbrain")

UpvalueHacker.SetUpvalue(CrabkingClawBrain.OnStart, New_ShouldClamp, "ShouldClamp")
