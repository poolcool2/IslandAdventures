# Island Adventures

### A modification for Don't Starve Together

The aim of this mod is to bring the content from the Don't Starve DLC "Shipwrecked" to Don't Starve Together, also adapting and tuning it to suit multiplayer better. All of the content is based on Shipwrecked, there are no completely new features included.

>Don’t let the tropical breeze lull you into a false sense of security - the world might be different, but it is still equal parts unforgiving and uncompromising. You will quickly find these islands are full to the brim with things that want to kill you.

## The Team

- Community Planning: **Chris** 
- Programmers:
	- **Mobbstar**
	- **Zarklord**
	- **ZupaleX** (retired)
	- **Ysovuka/Kzisor** (retired)
	- **CodeCombustion** (retired)
- Animators & Artists:
	- **Goat Slice**
	- **HalfRose** (retired)
- Translators/Writers:
	- **ImDaMisterL**
	- **Chris**
- New Logo: **Goat Slice**
- Old Logo: **Spiderdian** (retired)

## Installation

You can install the mod from GitLab by clicking the cloud button on the repo homepage like so:
![image](https://cdn.discordapp.com/attachments/278017358692352000/477627110723158027/unknown.png)
Unpack the ZIP file in your DST mod folder (usually " C:\Program Files (x86)\Steam\steamapps\common\Don't Starve Together\mods"). If you can't find your DST folder, use Steam to "browse local files".
Lastly, enable the mod when starting a server. Clients will automatically enable the mod when joining an IA server.

This mod requires the Gem Core API: https://gitlab.com/DSTAPIS/GemCore/wikis/Home

Please remember that the GitLab version may be unstable.

## Contribution

Do you want to support Island Adventures? Whatever you can do to help this mod grow and improve, please go ahead and contact us on our public Discord server ( https://discord.gg/8HJMq4h ), GitLab or the Steam Workshop page ( https://steamcommunity.com/sharedfiles/filedetails/?id=1467214795 )

Command line instructions (by GitLab)
git config --global user.name "My Username"
git config --global user.email "myaddress@jmail.com"


----

This mod contains assets from the game extension Shipwrecked for Don't Starve. Shipwrecked has been created by Klei Entertainment and Capybara Games. No copyright infringement intended.
